package com.example.ciapp.sample2;
import java.util.HashMap;
public class LoginManager {
    private HashMap<String, User> users;

    public LoginManager() {
        this.users = new HashMap<>();
    }

    public void register(String username, String password) throws ValidateFailedException{
        if (!isUsernameValid(username)) {
            throw new ValidateFailedException("Username input is invalid");
        }
        if (!isPasswordValid(password)) {
            throw new ValidateFailedException("Password input is invalid");
        }

        users.put(username, new User(username, password));
    }

    private boolean isUsernameValid(String username){
        if (username.matches("^.*[a-z].*$")) {
            if (username.matches("^.*[0-9].*$")) {
                if (username.matches("^.*[A-Z].*$")) {
                    return username.matches("^.{4,}");
                }
            }
        }
        return false;
    }

    private boolean isPasswordValid(String password){
        if(password.matches("^[a-zA-Z0-9]{8,}$")){
            return password.matches(".*[A-Z].*");
        }
        return false;
    }

    public User login(String username, String password)throws Exception
    {
        if(users.containsKey(username)){
            if(users.get(username).getPassword().equals(password)){
                return users.get(username);
            } else  {
                throw  new InvalidPasswordException("Password not match");
            }
        } else {
            throw  new UserNotFoundException("User does not exists");
        }
    }}
