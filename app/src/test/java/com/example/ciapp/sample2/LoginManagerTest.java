package com.example.ciapp.sample2;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class LoginManagerTest {
    private LoginManager loginManager;

    @Before
    public void setUp() throws ValidateFailedException{
        loginManager = new LoginManager();
        loginManager.register("Testuser1", "Password12");
    }

    @Test
    public void testLoginSuccess() throws Exception {
        User user = loginManager.login("Testuser1", "Password12");
        assertThat(user.getUsername(), is("Testuser1"));
        assertThat(user.getPassword(), is("Password12"));
    }

    @Test(expected = InvalidPasswordException.class)
    public void testLoginWrongPassword() throws Exception {
        User user = loginManager.login("Testuser1", "1234");
    }

    @Test(expected = UserNotFoundException.class)
    public void testLoginUnregisteredUser() throws Exception {
        User user = loginManager.login("iniad", "Password12");
    }

}